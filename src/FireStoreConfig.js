import * as firebase from 'firebase';
import firestore from 'firebase/firestore'

const settings  = {timestampsInSnapshots: true};

const config = {
    apiKey: "AIzaSyDWd35c38Oy1j9wjW_hQW0Z3AUT9WeQV_Q",
    authDomain: "sloop-3e9c8.firebaseapp.com",
    databaseURL: "https://sloop-3e9c8.firebaseio.com",
    projectId: "sloop-3e9c8",
    storageBucket: "sloop-3e9c8.appspot.com",
    messagingSenderId: "1014626278413"
};

firebase.initializeApp(config);

firebase.firestore().settings(settings);

export default firebase;