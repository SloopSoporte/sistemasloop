import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import firebase from './FireStoreConfig'
import './App.css';
import Home from './Components/Home'

class App extends Component {
  render() {
    return (
      <div class="container">
        <Home/>
      </div>
    );
  }
}

 export default App;