import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import Edit from './Components/Edit'
import Create from './Components/Create'
import Show from './Components/Show'
import { BrowserRouter, Switch, } from 'react-router-dom';
import Home from './Components/Home'


ReactDOM.render(
<Router>
    <div>
        <Route exact path='/' component={App}/>
        <Route path='/Home' component={Home}/>
        <Route path='/edit/:id' component={Edit}/>
        <Route path='/create' component={Create}/>
        <Route path='/show/:id' component={Show}/>
    </div>
</Router>
/*<APPLOG/>*/, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
