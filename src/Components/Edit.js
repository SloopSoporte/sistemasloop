import React, { Component } from 'react'
import firebase from '../FireStoreConfig'
import { Link } from 'react-router-dom'

class Edit extends Component {
    
    constructor(props) {
        super(props);
            this.state = {
                key: '',
                Apellidos:'',
                Nombres:'',
                Password:'',
                email:''
            };
    }
    
    componentDidMount(){
        const ref = firebase.firestore().collection('SystemUsers').doc(this.props.match.params.id);
        ref.get().then((doc) => {
            if (doc.exists) {
                const SystemUser = doc.data();
                this.setState({
                    key: doc.id,
                    Apellidos: SystemUser.Apellidos,
                    Nombres: SystemUser.Nombres,
                    Password: SystemUser.Password,
                    email: SystemUser.email
                });
            } else{
                console.log("No existe el usuario!");
            }
        });
    }

    onChange = (e) => {
        const state = this.state
        state[e.target.name] = e.target.value;
        this.setState({SystemUser:state});
    }

    onSubmit = (e) => {
        
        e.preventDefault();

        const { Apellidos, Nombres, Password, email} = this.state;

        const updateRef = firebase.firestore().collection('SystemUsers').doc(this.state.key);
        updateRef.set({
            Apellidos, 
            Nombres, 
            Password,
            email
        }).then((docRef) => {
            this.setState({
                key: '',
                Apellidos: '',
                Nombres: '',
                Password:'',
                email:''
            });
            this.props.history.push("/show/"+this.props.match.params.id)
        }).catch((error) => {
            console.error("Error al cargar al Usuario: ",error);
        });

        var user = firebase.auth().currentUser;
      
        user.updateEmail(email).then(function() {
            // Update successful.
        }).catch(function(error) {
            // An error happened.
        });
        user.updatePassword(Password).then(function() {
            // Update successful.
        }).catch(function(error) {
            // An error happened.
        });

        alert('se guardaron los cambios exitosamente.');
    }
    render(){
        return(
            <div className="container">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">
                            Editar Usuario
                        </h3>
                    </div>
                    <div className="panel-body">
                        <h4><Link to={`/show/${this.state.key}`} className="btn btn-primary">Regresar</Link></h4>
                        <h4><Link to={`/`} className="btn btn-primary">Lista de Usuarios</Link></h4>
                        <form onSubmit={this.onSubmit}>
                            <div className="form-group">
                                <label for="Nombres">Nombre:</label>
                                <input type="text" className="form-control" name="Nombres" value={this.state.Nombres}
                                onChange={this.onChange} placeholder="Nombres"/>
                            </div>
                            <div className="form-group">
                                <label for="Apellidos">Apellidos:</label>
                                <input type="text" className="form-control" name="Apellidos" value={this.state.Apellidos}
                                onChange={this.onChange} placeholder="Apellidos"/>
                            </div>
                            <div className="form-group">
                                <label for="email">email:</label>
                                <input type="email" className="form-control" name="email" value={this.state.email}
                                onChange={this.onChange} placeholder="Correo Electronico"/>
                            </div>
                            <div className="form-group">
                                <label for="Password">Password:</label>
                                <input type="password" className="form-control" name="Password" value={this.state.Password}
                                onChange={this.onChange} placeholder="Password"/>
                            </div>
                            <button type="submit" className="btn btn-light" > Guardar</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

}

export default Edit;