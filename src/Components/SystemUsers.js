import React, { Component } from 'react'
import db from '../FireStoreConfig'
import { Table, Button } from 'reactstrap'

export default class SystemUsers extends Component {
    state ={
        Apellidos:'',
        Nombres:'',
        Password:'',
        Username:''
    }
    componentDidMount(){
        db.collection('SystemUsers').get().then((snapShots)=>{
            this.setState({
                Usuarios:snapShots.docs.map(doc => {
                    return { id: doc.id,data:doc.data()}
                })
            })
        }, error =>{
            console.log(error)
        });
    }
  render() {
      const { Users } = this.state;
    return (
      <div>
        <Table hover className="text-center">
            <thead>
                <tr>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                </tr>
            </thead>
            <tbody>
                { Users && Users !== undefined ? Users.map((User,key)=>(
                    <tr key={key}>
                        <td>{User.data.User}</td>
                        <td><Button color="warning">Editar</Button></td>
                        <td><Button color="danger">Eliminar</Button></td>
                    </tr>
                )):null
                }
            </tbody>
        </Table>
      </div>
    )
  }
}
