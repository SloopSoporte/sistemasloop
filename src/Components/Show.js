import React, { Component } from 'react';
import firebase from '../FireStoreConfig';
import { Link } from 'react-router-dom';

class Show extends Component{

    constructor(props){
        super(props);
        this.state = {
            SystemUsers: {},
            key: ''
        };
    }

    componentDidMount(){
        const ref = firebase.firestore().collection('SystemUsers').doc(this.props.match.params.id);
        ref.get().then((doc) => {
            if(doc.exists) {
                this.setState({
                    SystemUsers: doc.data(),
                    key: doc.id,
                    isLoading: false
                });
            }else {
                console.log("No existe el usuario");
            }
        });
    }

    delete(id){
        var user = firebase.auth().currentUser;

        user.delete().then(function() {
        // User deleted.
        }).catch(function(error) {
        // An error happened.
        });
        firebase.firestore().collection('SystemUsers').doc(id).delete().then(() => {
            console.log("se ah eliminado al usuario");
            this.props.history.push("/")
        }).catch((error) => {
            console.error("No se pudo eliminar al usuario: ", error);
        });
        alert('se elimino al usuario correctamente.')
    }
    login(email, password){
        firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // ...
          });
          var user = firebase.auth().currentUser;
          if(user!= null){
            alert('estas logeado');
          }
          console.log(user);
    }
    Logout(){
        var user = firebase.auth().currentUser;
        firebase.auth().signOut().then(function() {
            // Sign-out successful.
        }).catch(function(error) {
            // An error happened.
        });
        if(user == null){
            alert('se ah deslogeado el usuario');
        }
    }
    check(){
        var user = firebase.auth().currentUser;
        if(user != null){
            alert('el usuario esta logeado');
        }else{
            alert('el usuario no esta logeado');
        }

    }

    render(){
        return(
            <div className="container">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <br/>
                        <button className="btn btn-warning"><Link to="/">Users List</Link></button>
                        <br/>
                        <br/>
                        <h3 className="panel-title">
                            {this.state.SystemUsers.Nombres}
                        </h3>
                    </div>
                    <div className="panel-body">
                        <dl>
                            <dt>Nombres:</dt>
                            <dd>{this.state.SystemUsers.Nombres}</dd>
                            <dt>Apellidos:</dt>
                            <dd>{this.state.SystemUsers.Apellidos}</dd>
                            <dt>Email:</dt>
                            <dd id="email">{this.state.SystemUsers.email}</dd>
                            <dt>Password</dt>
                            <dd type="password" id="password">{this.state.SystemUsers.Password}</dd>

                        </dl>
                        <Link to={`/edit/${this.state.key}`} className="btn btn-success">Edit</Link>&nbsp;
                        <button onClick={this.delete.bind(this, this.state.key)} className="btn btn-light"><Link to="/">Eliminar</Link></button>
                        <button onClick={this.login.bind(this, this.state.SystemUsers.email,
                         this.state.SystemUsers.Password)} className="btn btn-info">Login</button>
                        <button onClick={this.Logout} className="btn btn-info">Logout</button>
                        <button onClick={this.check} className="btn btn-light">check</button>
                    </div>
                </div>
            </div>
        );
    }

}

export default Show;