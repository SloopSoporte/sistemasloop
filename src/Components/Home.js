import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import firebase from '../FireStoreConfig'

class Home extends Component {
  constructor(props){
    super(props);
    this.ref = firebase.firestore().collection('SystemUsers');
    this.unsubscribe = null;
    this.state = {
      SystemUsers: []
    };
  }

  onCollectionUpdate = (querySnapshots) => {
    const SystemUsers = [];
    querySnapshots.forEach((doc) => {
      const { Apellidos, Nombres, Password, email } = doc.data();
      SystemUsers.push({
        key: doc.id,
        doc,
        Apellidos,
        Nombres,
        Password,
        email
      });
    });
    this.setState({
      SystemUsers
    });
  }

  componentDidMount(){
    this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
  }

  render() {
    return (
      <div class="container">
        <div class="plan panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              Lista de Doctores.
            </h3>
          </div>
          <div class="panel-body">
            <button className="btn btn-light"><Link to="/create">Agregar Usuario</Link></button>
            <table className="table table-stripe">
              <thead>
                <tr>
                  <th>Nombres</th>
                  <th>Apellidos</th>
                  <th>Email</th>
                </tr>
              </thead>
              <tbody>
                {this.state.SystemUsers.map(SystemUser => 
                  <tr>
                    <td>{SystemUser.Nombres}</td>
                    <td>{SystemUser.Apellidos}</td>
                    <td>{SystemUser.email}</td>
                    <button className="btn btn-light"><Link  to={`/show/${SystemUser.key}`}>Mostrar</Link></button>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

 export default Home;