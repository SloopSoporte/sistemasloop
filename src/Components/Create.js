import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import firebase from '../FireStoreConfig'
import { Link } from 'react-router-dom'

class Create extends Component {

    constructor(){
        super();
        this.ref = firebase.firestore().collection('SystemUsers');
        this.state = {
            Apellidos: '',
            Nombres: '',
            Password: '',
            email:''
        };
    }

    onChange = (e) => {
        const state = this.state
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    onSubmit = (e) => {
        e.preventDefault();
        const { Apellidos, Nombres, Password, email } = this.state;
        
        this.ref.add({
            Apellidos,
            Nombres,
            Password,
            email
        }).then((docRef) => {
            this.setState({
                Apellidos: '',
                Nombres: '',
                Password: '',
                email:''
            });
            this.props.history.push('/')
        }).catch((error) =>{
            console.error("Error adding User: ", error);
        });
    }

    register = (e) => {
        firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.Password)
        .catch(function(error){
          var errorCode = error.code;
          var errorMessage = error.message;
          if ( errorCode == 'auth/weak-password') {
            alert('The password is too weak.'); 
          } else {
            alert(errorMessage);
          }
        });
        alert('se registro el usuario, de click en user list')
      }

    render(){
        const { Apellidos, Nombres, Password, email } = this.state;
        return(
            <div className="container">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-tittle">
                            Add User
                        </h3>
                    </div>
                    <div className="panel-body">
                        <h4><Link to="/" className="btn btn-primary">Users list</Link></h4>
                        <form onSubmit={this.onSubmit}>
                            <div className="form-group">
                                <label for="Nombres">Nombres:</label>
                                <input type="text" className="form-control" name="Nombres"
                                value={Nombres} onChange={this.onChange} placeholder="Nombres"/>
                            </div>
                            <div className="form-group">
                                <label for="Apellidos">Apellidos:</label>
                                <input type="text" className="form-control" name="Apellidos"
                                value={Apellidos} onChange={this.onChange} placeholder="Apellidos"/>
                            </div>
                            <div className="form-group">
                                <label for="email">email:</label>
                                <input type="email" className="form-control" name="email"
                                value={email} onChange={this.onChange} placeholder="Correo Electronico"/>
                            </div>
                            <div className="form-group">
                                <label for="Password">Password:</label>
                                <input type="password" className="form-control" name="Password"
                                value={Password} onChange={this.onChange} placeholder="Password"/>
                            </div>
                            <button type="submit" className="btn btn-success" onClick={this.register}>Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }


}

export default Create;